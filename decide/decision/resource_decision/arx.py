from controller import Controller
from decide.utils.config import ARXConfig, DefaultConfig
from commons.utils.config import MachineConfig


class ARX(Controller):
    def __init__(self, power_init, power_cap):
        Controller.__init__(self)
        self.__config = ARXConfig()
        self.__quota_min = self.__config.quota_min
        self.__quota_max = MachineConfig().cores * 100
        self.__a = self.__config.a
        self.__b = self.__config.b
        self.__p = self.__config.p

        self.__old_epsilon = (power_cap - power_init) / self.__config.power_sd

    def decide(self, power_cap, power_current, resources):
        quota = Controller.normalize_resources(resources, self.__config.quota_avg, self.__config.quota_sd)

        epsilon = (power_cap - power_current)/self.__config.power_sd

        # model R(t) = R(t-1) + (1-p)*E(t)/b -(1-p)*a*E(t-1)/b
        quota += (epsilon - self.__old_epsilon * self.__a) * (1 - self.__p) / self.__b
        self.__old_epsilon = epsilon

        # denormalize quota
        percentage_quota = quota * self.__config.quota_sd + self.__config.quota_avg

        # keep the quota between quota_min and quota_max
        if percentage_quota > self.__quota_max:
            percentage_quota = self.__quota_max
        elif percentage_quota < self.__quota_min:
            percentage_quota = self.__quota_min

        return percentage_quota
