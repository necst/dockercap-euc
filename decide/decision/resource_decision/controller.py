import abc
import copy

from commons.utils.config import MachineConfig


class Controller:
    __metaclass__ = abc.ABCMeta

    def __init__(self):
        pass

    @abc.abstractmethod
    def decide(self, power_cap, power_current, resources):
        pass

    @staticmethod
    def get_resources(containers):
        resources = {}

        for container in containers:
            resources[container["id"]] = {}
            resources[container["id"]]["cpu_quota"] = container["cpu_quota"]
            resources[container["id"]]["cpu_period"] = container["cpu_period"]

        return resources

    @staticmethod
    def normalize_resources(resources, mean, sd):
        number_cores = MachineConfig().cores
        normalized_resources = copy.deepcopy(resources)
        # count the total resources allocated
        total = 0.0
        for container in normalized_resources.keys():
            normalized_resources[container]["cpu_quota"] = normalized_resources[container]["cpu_quota"] * 100 / float(normalized_resources[container]["cpu_period"])
            total += normalized_resources[container]["cpu_quota"]

        # fix the hardlimits of resources
        if total > number_cores*100:
            for container in normalized_resources.keys():
                normalized_resources[container]["cpu_quota"] = normalized_resources[container]["cpu_quota"] * number_cores * 100 / total

        # normalize the resources w.r.t. the model avg and sd
        total = 0
        for container in normalized_resources.keys():
            total += normalized_resources[container]["cpu_quota"]

        return (total - mean)/sd

    @staticmethod
    def denornalize_resources(resources):
        percent_resources = resources

        for container in percent_resources:
            percent_resources[container]["cpu_quota"] *= 100

        return percent_resources
