import sys
import subprocess

from Queue import Queue
from observe.observe_component import ObserveComponent
from decide.decide_component import DecideComponent
from act.act_component import ActComponent

with open("cap_stop", "wb") as cap_stop:
	cap_stop.write("0")

observe_queue = Queue()
act_queue = Queue()

oc = ObserveComponent(observe_queue)
dc = DecideComponent(observe_queue, act_queue)
ac = ActComponent(act_queue)

oc.start()
dc.start()
ac.start()

while True:
	with open("cap_stop", "r") as cap_stop:
		val = cap_stop.read()

		if val == "1":
			print "Terminating DockerCap..."
			oc.stop()
			dc.stop()
			ac.stop()
			
			oc.join()
			dc.join()
			ac.join()
			print "Components terminated"
			sys.exit()
	
	subprocess.call(["sleep", str(3)])