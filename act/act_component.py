import json
import time
import os
import sys

from threading import Thread, Event
from Queue import Empty

from commons.utils.json_decoder import decode_dict
from commons.interfaces.cgroup_interface import CGroupInterface

class ActComponent(Thread):
	"""Act Component of DockerCap"""

	def __init__(self, act_queue):
		Thread.__init__(self)
		
		self.__act_queue = act_queue
		self._stop = Event()
	
	def stop(self):
		self._stop.set()
	
	def stopped(self):
		return self._stop.isSet()

	def run(self):
		while not self.stopped():
			read = True
			try:
				resources = self.__act_queue.get(block = True, timeout=3)
			except Empty:
				read = False
			if read:
				print "ACT" + str(resources)
				try:
					self.act_resources(resources["resources"])
				except RuntimeError:
					print "No more container to control"

		print "Terminating Act Component"


	@staticmethod
	def act_resources(resources):
		for container in resources.keys():
		    try:
		    	CGroupInterface.set_cpu_quota(value=resources[container]["cpu_quota"], container_id=container)
		        print container + ": " + str(resources[container]["cpu_quota"])
		    except ValueError:
		        print "Container " + str(container) + " not found"
		        resources.pop(container)
		if len(resources.keys()) == 0:
			raise RuntimeError("No more container available")
