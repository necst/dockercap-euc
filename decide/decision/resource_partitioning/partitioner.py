import abc
import copy

from decide.utils.config import PartitionerConfig


class Partitioner:
    HIGH_PRIORITY = "high_priority"
    LOW_PRIORITY = "low_priority"
    BEST_EFFORT = "best_effort"

    __metaclass__ = abc.ABCMeta

    def __init__(self):
        self._config = PartitionerConfig()

    @abc.abstractmethod
    def partition(self, resources, global_quota):
        pass

    @staticmethod
    def _build_priority_list(resources, profile):
        running_priority_list = {Partitioner.HIGH_PRIORITY: [], Partitioner.LOW_PRIORITY: [], Partitioner.BEST_EFFORT: []}
        for container_id in resources.keys():
            i = False
            for priority in profile:
                if resources[container_id]["image"] == priority["image"] and resources[container_id]["command"] == priority["command"]:
                    if priority["priority"] == Partitioner.BEST_EFFORT:
                        running_priority_list[Partitioner.BEST_EFFORT].append(container_id)
                    else:
                        running_priority_list[priority["priority"]].append(
                            {"id": container_id, "K": priority["K"], "constraint": priority["constraint"]})
                    i = True
                    break
            if i is False:
                running_priority_list[Partitioner.BEST_EFFORT].append(container_id)
        print "High priority:" + str(running_priority_list[Partitioner.HIGH_PRIORITY])
        print "Low priority:" + str(running_priority_list[Partitioner.LOW_PRIORITY])
        print "Best effort:" + str(running_priority_list[Partitioner.BEST_EFFORT])

        return running_priority_list
