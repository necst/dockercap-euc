import abc


class Decider:
    __metaclass__ = abc.ABCMeta

    def __init__(self, power_init, power_cap):
        self._power_init = power_init
        self._power_cap = power_cap

    @abc.abstractmethod
    def decide(self, power_cap, power_current, containers):
        pass

    @staticmethod
    def get_resources(containers):
        resources = {}

        for container in containers:
            resources[container["id"]] = {}
            resources[container["id"]]["cpu_quota"] = container["cpu_quota"]
            resources[container["id"]]["cpu_period"] = container["cpu_period"]
            resources[container["id"]]["image"] = container["image"]
            resources[container["id"]]["command"] = container["command"]
        return resources
