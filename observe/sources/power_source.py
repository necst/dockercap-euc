import abc

class PowerSource:
	__metaclass__ = abc.ABCMeta

	def __init__():
		pass

	@staticmethod
	@abc.abstractmethod
	def get_samples(num_sample):
		pass


