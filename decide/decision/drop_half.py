from decider import Decider
from resource_decision.controller import Controller
from decide.utils.config import ARXConfig

class DropHalf(Controller):
    def __init__(self):
        Controller.__init__(self)
        self.__config = ARXConfig()

    def decide(self, power_cap, power_current, containers):
        resources = Decider.get_resources(containers)
        quota = Controller.normalize_resources(resources, self.__config.power_avg, self.__config.power_sd)

        # count the total for the %
        total = 0
        for container in resources.keys():
            total += resources[container]["cpu_quota"]

        # drop half
        quota *= 0.5

        # denormalize
        denormalized_quota = quota * self.__config.power_sd + self.__config.power_avg

        resources_result = resources

        for container in resources_result.keys():
            resources_result[container["cpu_quota"]] = denormalized_quota * resources[container]["cpu_quota"] / total
        return resources_result

