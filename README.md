# README #
This is the official repository of the project DockerCap. DockerCap is a software-level power capping orchestrator for Docker containers that follows an Observe-Decide-Act loop structure. 

## Getting started ##
DockerCap was developed and tested with the underlying configuration:

* Python 2.7.6
* Intel Xeon E5-1410 processor
* Ubuntu 14.04 with the Linux kernel 3.19.0-42
* Docker 1.9.0

To start DockerCap, run the following script with sudo privileges

```
#!bash

sudo python dockercap.py
```

The system will automatically detects the running Docker containers and tune them following the configuration specified.

The Dockerfile of the benchmarks used can be founded here:
[Link Text](https://hub.docker.com/r/spirals/parsec-3.0/)

To stop DockerCap, run the following script with sudo privileges
```
#!bash

sudo python stop_dockercap.py
```

## Configuration ##
Each module has its own configuration file

### Shared Configuration
The shared configuration file is located in
```
#!path

commons/config.xml
```
* cgroup path: the path of the cgroup directory
* cgroup group: the cgroup hierarchy that its build from a docker container. Place the "?CONTAINER_ID?" placeholder where there should be the specific container id

### Observe Component
The configuration file is located in 
```
#!path

observe/config.xml
```

* RAPL sample_time: the period of time of a RAPL sampling [ms]
* num_sample: the number of samples fetched before passing the information to the Decide Component 

### Decide Component
The configuration file is located in 
```
#!path

decide/config.xml
```
* controller: the desired controlling and partitioning policy
* power_cap: the desired power cap
* output: the output directory in which the logs will be written
* arx : all the parameters bounded to the ARX controller
* partitioner weight high and low: weight used in the Priority based partitioner
* partitioner quota_container_min: the minimum amount of cpu quota that it is given to each container possibly

## Licence ##
Copyright (c) 2016 Politecnico di Milano

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.