import copy

from decider import Decider
from resource_decision.arx import ARX
from resource_partitioning.partitioner_priority_throughput import PartitionerPriorityThroughputAware
from decide.utils.config import ARXConfig


class ARXPriorityThroughputAware(Decider):
    def __init__(self, power_init, power_cap, profile):
        Decider.__init__(self, power_init, power_cap)

        self.__profile = profile

        self.__controller = ARX(self._power_init, self._power_cap)
        self.__partitioner = PartitionerPriorityThroughputAware(self.__profile)

    def decide(self, power_cap, power_current, containers):
        resources = self.get_resources(containers)

        arx_quota = self.__controller.decide(power_cap, power_current, resources)

        return self.__partitioner.partition(resources,arx_quota)