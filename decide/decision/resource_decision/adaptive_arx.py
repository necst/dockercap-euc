import numpy
from controller import Controller
from decide.utils.config import ARXConfig, RLSConfig, DefaultConfig
from commons.utils.config import MachineConfig


class AdaptiveARX(Controller):
    def __init__(self, power_init, power_cap):
        Controller.__init__(self)
        self.__arx_config = ARXConfig()

        self.__quota_min = DefaultConfig().quota_min
        self.__quota_max = MachineConfig().cores * 100
        self.__a = self.__arx_config.a
        self.__b = self.__arx_config.b
        self.__RLS = self.RLS()

        self.__p = self.__arx_config.p
        self.__old_power = power_init
        self.__old_epsilon = (power_cap - power_init) / self.__arx_config.power_sd

    def decide(self, power_cap, power_current, resources):
        quota = Controller.normalize_resources(resources, self.__arx_config.quota_avg, self.__arx_config.quota_sd)

        # z-norm of old power consumption
        old_power_norm = (self.__old_power - self.__arx_config.power_avg) / self.__arx_config.power_sd

        # z-norm of current power consumption
        current_power_norm = (power_current - self.__arx_config.power_avg) / self.__arx_config.power_sd

        # RLS weight update
        self.__a, self.__b = self.__RLS.get_weights(quota, old_power_norm, current_power_norm)
        print "a:" + str(self.__a) + ", b:" + str(self.__b)
        epsilon = (power_cap - power_current)/self.__arx_config.power_sd

        # model R(t) = R(t-1) + (1-p)*E(t)/b -(1-p)*a*E(t-1)/b
        quota += (epsilon - self.__old_epsilon * self.__a) * (1 - self.__p) / self.__b
        self.__old_epsilon = epsilon
        self.__old_power = power_current

        # denormalize quota
        percentage_quota = quota * self.__arx_config.quota_sd + self.__arx_config.quota_avg

        # keep the quota between quota_min and quota_max
        if percentage_quota > self.__quota_max:
            percentage_quota = self.__quota_max
        elif percentage_quota < self.__quota_min:
            percentage_quota = self.__quota_min

        return percentage_quota

    class RLS:
        def __init__(self):
            self.__arx_config = ARXConfig()
            self.__rls_config = RLSConfig()
            
            self.__a = self.__arx_config.a
            self.__b = self.__arx_config.b
            self.__lambda = self.__rls_config.lmd
            self.__delta = self.__rls_config.delta

            self.__P = self.__delta * numpy.identity(2)
            self.__w = numpy.array([[self.__a], [self.__b]])

        def get_weights(self, quota, old_power, current_power):
            u = numpy.array([[old_power], [quota]])
            d = current_power

            pi = u.transpose().dot(self.__P)
            gamma = self.__lambda + pi.dot(u)
            k = 1/gamma * pi.transpose()
            epsilon = d - self.__w.transpose().dot(u)
            self.__w = self.__w + k * epsilon
            Pprime = k.dot(pi)
            self.__P = 1/self.__lambda * (self.__P - Pprime)

            return self.__w[0][0], self.__w[1][0]
