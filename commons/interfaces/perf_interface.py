import subprocess as sp
import os


class PerfInterface:
    RAPL_PACKAGE = "power/energy-pkg/"
    __file_name = "tmp.csv"

    @staticmethod
    def perf_stat(events, time_sample, total_time, file_name=__file_name):
        perf_bash = "perf stat -e " + events + \
                    " -o " + file_name + \
                    " -a -x, -I " + str(time_sample) + \
                    " sleep " + str(total_time)

        sp.call(perf_bash.split())

        records = PerfInterface.__parse_loop_out(file_name=file_name)

        os.remove(file_name)
        return records

    @staticmethod
    def __parse_loop_out(file_name=__file_name):
        records = []
        with open(file_name, "r") as file_in:
            reader = file_in.readlines()
            for line in reader:
                spacings = line.split(" ")
                while True:
                    try:
                        spacings.remove("")
                    except ValueError:
                        break
                if spacings[0] != "#" and spacings[0] != "\n":
                    elements = spacings[0].split(",")
                    records.append({"time": elements[0], elements[3].strip(): elements[1], "unit": elements[2]})

        return records
