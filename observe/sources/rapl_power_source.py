from power_source import PowerSource
from observe.utils.config import RAPLConfig
from commons.interfaces.perf_interface import PerfInterface

class RAPLPowerSource(PowerSource):
    __config = RAPLConfig()

    def __init__(self):
        pass

    @staticmethod
    def get_samples(num_sample):
        values = []
        sample_time = RAPLPowerSource.__config.sample_time
        total_time = sample_time * num_sample * 0.001
        result = PerfInterface.perf_stat(PerfInterface.RAPL_PACKAGE, sample_time, total_time)
        time = 0
        i = 0
        for element in result:
            if i < num_sample:
                values.append(float(element[PerfInterface.RAPL_PACKAGE])/(float(element["time"])-time))
                time = float(element["time"])
            i += 1

        return values