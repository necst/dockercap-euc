from commons.interfaces.cgroup_interface import CGroupInterface
from commons.interfaces.docker_interface import DockerInterface
from commons.utils.config import MachineConfig

class ResourcesSource:
    __machine_config = MachineConfig()

    def __init__(self):
        pass

    @staticmethod
    def get_resources():        
        containers = DockerInterface.ps()

        for container in containers:
            try:
                print "container " + container["id"] + " detected"
                container["cpu_period"] = CGroupInterface.get_cpu_period(container["id"])
                container["cpu_quota"] = CGroupInterface.get_cpu_quota(container["id"])

                if container["cpu_quota"] == -1:
                    container["cpu_quota"] = container["cpu_period"] * ResourcesSource.__machine_config.cores

            except ValueError:
                print "Container " + str(container["id"]) + " not found"
                containers.remove(container)

        if len(containers) == 0:
            raise RuntimeError("No more container available")

        return containers
