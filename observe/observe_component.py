import subprocess
import json
import time
import sys

from threading import Thread, Event

from sources.rapl_power_source import RAPLPowerSource
from sources.resources_source import ResourcesSource
from utils.config import DefaultConfig

class ObserveComponent(Thread):
	"""Observe Component of DockerCap"""

	def __init__(self, queue):
		Thread.__init__(self)
		self.__queue = queue
		self.__power_source = RAPLPowerSource()
		self.__resources_source = ResourcesSource()
		self.__config = DefaultConfig()
		self._stop = Event()
	
	def stop(self):
		self._stop.set()
	
	def stopped(self):
		return self._stop.isSet()

	@staticmethod
	def __sleep(sleep_time):
		if sleep_time is not 0:
			subprocess.call(["sleep", str(sleep_time)])

	def run(self):
		while not self.stopped():
			to_enqueue = True
			power_sample = self.__power_source.get_samples(self.__config.num_sample)[0]
			try:
				resources = self.__resources_source.get_resources()
			except RuntimeError:
				print "no container detected"
				to_enqueue = False
				self.__sleep(3)

			if to_enqueue is True:
				self.__queue.put({"time": time.time(), "power": power_sample, "resources": resources})

		print "Terminating Observe Component"
