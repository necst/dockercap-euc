import copy

from decider import Decider
from resource_decision.adaptive_arx import AdaptiveARX


class AdaptiveARXFairController(Decider):
    def __init__(self, power_init, power_cap):
        Decider.__init__(self, power_init, power_cap)
        self.__arx = AdaptiveARX(self._power_init, self._power_cap)

    def decide(self, power_cap, power_current, containers):
        resources = self.get_resources(containers)

        percentage_quota = self.__arx.decide(power_cap, power_current, precision, resources)
        # fair resource split
        resources_result = copy.deepcopy(resources)

        count = len(resources.keys())
        for container in resources_result.keys():
            resources_result[container]["cpu_quota"] = percentage_quota / count

        return resources_result
