import copy
from partitioner import Partitioner

class PartitionerPriorityAware(Partitioner):
    def __init__(self, profile):
        Partitioner.__init__(self)
        self.__profile = profile

    def partition(self, resources, global_quota):
        priority_list = Partitioner._build_priority_list(resources, self.__profile)
        resources_result = copy.deepcopy(resources)

        container_weight = {}
        tot_weight = 0
        for profile in priority_list[Partitioner.HIGH_PRIORITY]:
            container_weight[profile["id"]] = self._config.weight_high
            tot_weight += self._config.weight_high
        for profile in priority_list[Partitioner.LOW_PRIORITY]:
            container_weight[profile["id"]] = self._config.weight_low
            tot_weight += self._config.weight_low
        for container in priority_list[Partitioner.BEST_EFFORT]:
            container_weight[container] = 1
            tot_weight += 1

        for container in resources_result.keys():
            resources_result[container]["cpu_quota"] = global_quota * (float(container_weight[container]) / tot_weight)

        return resources_result
