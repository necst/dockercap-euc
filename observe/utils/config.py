import os
from xml.etree import ElementTree

class Config:
    def __init__(self):
        self.update()

    def update(self):
        self.tree = ElementTree.parse(os.path.abspath("observe/config.xml"))

class RAPLConfig(Config):
	
	def __init__(self):
		Config.__init__(self)

		self.update()

	def update(self):
		Config.update(self)

		rapl = self.tree.getroot().find("rapl")
		self.__sample_time = int(rapl.find("sample_time").text)

	@property
	def sample_time(self):
	    return self.__sample_time

class WattsupConfig(Config):
	def __init__(self):
		Config.__init__(self)
		self.update()

	def update(self):
		Config.update(self)

		wattsup = self.tree.getroot().find("watts-up")
		self.__device = wattsup.find("device").text
		self.__path = wattsup.find("path").text
		if self.__path is None:
			self.__path = ""
	
	@property
	def device(self):
	    return self.__device
	
	@property
	def path(self):
	    return self.__path

class DefaultConfig(Config):
	def __init__(self):
		Config.__init__(self)

		self.update()

	def update(self):
		Config.update(self)

		default = self.tree.getroot().find("default")
		self.__num_sample = int(default.find("num_sample").text)

	@property
	def num_sample(self):
		return self.__num_sample