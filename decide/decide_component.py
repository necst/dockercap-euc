import json
import time
import os
import socket
import csv
import sys

from threading import Thread, Event
from Queue import Empty

from commons.utils.json_decoder import decode_dict
from decide.utils.config import DefaultConfig

from decision.drop_half import DropHalf
from decision.arx_fair import ARXFairController
from decision.a_arx_fair import AdaptiveARXFairController
from decision.arx_priority import ARXPriorityAware
from decision.arx_priority_throughput import ARXPriorityThroughputAware

class DecideComponent(Thread):
	"""Decide Component of DockerCap"""

	def __init__(self, observe_queue, act_queue):
		Thread.__init__(self)
		
		self.__observe_queue = observe_queue
		self.__act_queue = act_queue

		self.__config = DefaultConfig()
		power_init = self.__config.power_init
		power_cap = self.__config.power_cap

		profile = self.__fetch_profile()

		controller_type = self.__config.controller
		if controller_type == self.__config.HALF:
			self.__controller = DropHalf()
		elif controller_type == self.__config.ARX_FAIR:
			self.__controller = ARXFairController(power_init, power_cap)
		elif controller_type == self.__config.ARX_PRIORITY:
			self.__controller = ARXPriorityAware(power_init, power_cap, profile)
		elif controller_type == self.__config.ARX_THROUGHPUT:
			self.__controller = ARXPriorityThroughputAware(power_init, power_cap, profile)
		elif controller_type == self.__config.ADAPTIVE_ARX_FAIR:
			self.__controller = AdaptiveARXFairController(power_init, power_cap)
		else:
			self.__controller = DropHalf()

		self.__power_cap = power_cap

		if not os.path.exists(self.__config.output):
			os.makedirs(self.__config.output)

		self.__file_name = self.__config.output + "/" + socket.gethostname() + "-dockercap-" + str(time.time()).split(".")[0] + ".csv"
		with open(self.__file_name, "wb") as fileout:
			wr = csv.DictWriter(fileout, fieldnames=["timestamp", "power", "resources", "config"])
			wr.writeheader()

		self._stop = Event()
	
	def stop(self):
		self._stop.set()
	
	def stopped(self):
		return self._stop.isSet()

	@staticmethod
	def __fetch_profile():
		with open(os.path.abspath("profile.json"), "r") as file:
			profile = json.load(file, object_hook=decode_dict)

		return profile

	def run(self):
		while not self.stopped():
			read = True
			try:
				record = self.__observe_queue.get(block = True, timeout=3)
			except Empty:
				read = False
			if read:
				resources = self.__controller.decide(self.__power_cap, record["power"], record["resources"])
				
				t = time.time()

				self.__act_queue.put({"time": t, "resources":resources})

				with open(self.__file_name, "a") as fileout:
					wr = csv.DictWriter(fileout, fieldnames=["timestamp", "power", "resources", "config"])
					wr.writerow({"timestamp": t, "power": record["power"], "resources": json.dumps(resources), "config": json.dumps(self.__config, default=DefaultConfig.serialize)})
 
		print "Terminating Decide Component"


