import copy
from partitioner import Partitioner

class PartitionerPriorityThroughputAware(Partitioner):
    def __init__(self, profile):
        Partitioner.__init__(self)
        self.__profile = profile

    def partition(self, resources, global_quota):
        priority_list = Partitioner._build_priority_list(resources, self.__profile)
        resources_result = copy.deepcopy(resources)

        # set the minimum amount of resources for each container
        tot_assigned_quota = 0
        print "Baseline assignment"
        for container in resources_result.keys():
            resources_result[container]["cpu_quota"] = self._config.quota_container_min
            tot_assigned_quota += self._config.quota_container_min
            print str(container) + ":" + str(resources_result[container]["cpu_quota"])
        baseline_quota = tot_assigned_quota

        # return the minimum resources for each container, you cannot go below the baseline
        if baseline_quota >= global_quota:
            return resources_result

        # assign the quota based on the priority: production > best effort > normal
        # production and best effort priority gain the quota that they needs to satisfy their constraints
        # normal priority gains the remain quota

        # high priority assigment
        print "High prioriyy astignment"
        for profile in priority_list[self.HIGH_PRIORITY]:
            assigned_quota = self.__get_constrainted_quota(profile)
            resources_result[profile["id"]]["cpu_quota"] = assigned_quota
            tot_assigned_quota += assigned_quota - self._config.quota_container_min
            print str(profile["id"]) + ":" + str(resources_result[profile["id"]]["cpu_quota"])
        # check if exceed quota - return the assigment with the remain resoruces if it happens
        if tot_assigned_quota >= global_quota:
            leftover_quota = global_quota - baseline_quota
            count = len(priority_list[self.HIGH_PRIORITY])
            for profile in priority_list[self.HIGH_PRIORITY]:
                resources_result[profile["id"]]["cpu_quota"] = leftover_quota / float(count)
            return resources_result

        baseline_quota = tot_assigned_quota

        # low priority assigment
        print "Low priority assignment"
        for profile in priority_list[self.LOW_PRIORITY]:
            assigned_quota = self.__get_constrainted_quota(profile)
            resources_result[profile["id"]]["cpu_quota"] = assigned_quota
            tot_assigned_quota += assigned_quota - self._config.quota_container_min
            print str(profile["id"]) + ":" + str(resources_result[profile["id"]]["cpu_quota"])

        # check if exceed quota - return the assigment with the remain resoruce if it happens
        if tot_assigned_quota >= global_quota:
            leftover_quota = global_quota - baseline_quota
            count = len(priority_list[self.LOW_PRIORITY])
            for profile in priority_list[self.LOW_PRIORITY]:
                resources_result[profile["id"]]["cpu_quota"] = leftover_quota / float(count)
            return resources_result

        # best effort
        print "Normal assignment"
        count = len(priority_list[self.BEST_EFFORT])
        leftover_quota = global_quota - tot_assigned_quota
        if leftover_quota > 0:
            for container in priority_list[self.BEST_EFFORT]:
                assigned_quota = leftover_quota / float(count)
                resources_result[container]["cpu_quota"] = assigned_quota
                tot_assigned_quota += assigned_quota - self._config.quota_container_min
                print str(container) + ":" + str(resources_result[container]["cpu_quota"])

        # check if there are any resources available. If so, distribute among all the container following priority

        # assign leftovers to high priority
        print "Leftover high priority"
        if tot_assigned_quota < global_quota:
            leftover_quota = global_quota - tot_assigned_quota
            count = len(priority_list[self.HIGH_PRIORITY])
            for profile in priority_list[self.HIGH_PRIORITY]:
                assigned_quota = leftover_quota / float(count)
                resources_result[profile["id"]]["cpu_quota"] += leftover_quota / float(count)
                tot_assigned_quota += assigned_quota
                print str(profile["id"]) + ":" + str(resources_result[profile["id"]]["cpu_quota"])

        # assign leftovers to low priority
        print "Leftover low priority"
        if tot_assigned_quota < global_quota:
            leftover_quota = global_quota - tot_assigned_quota
            count = len(priority_list[self.LOW_PRIORITY])
            for profile in priority_list[self.LOW_PRIORITY]:
                assigned_quota = leftover_quota / float(count)
                resources_result[profile["id"]]["cpu_quota"] += leftover_quota / float(count)
                tot_assigned_quota += assigned_quota
                print str(profile["id"]) + ":" + str(resources_result[profile["id"]]["cpu_quota"])

        return resources_result

    @staticmethod
    def __get_constrainted_quota(profile):
        return profile["K"] / float(profile["constraint"])